<!--
The MIT License (MIT)

Copyright (c) 2015 TECHMAUS

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

 -->

<!-- Hier komt alles samen, tevens bevat deze file de meta gegevens van de pagina -->

<?php include 'setup.php';?>

<!DOCTYPE html>
<head>

<title>Roosterwijzigingen van <?php echo $naamschool;?></title>

<meta charset="UTF-8">
<meta name="keywords" content="Dagrooster, roosterwijzingen, carmel, hengelo, rooster, uitvallen">
<meta name="author" content="TECHMAUS">

<link href='http://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400,400italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="/css/<?php echo $afkorting;?>/<?php echo $afkorting;?>.css">
<link rel="stylesheet" type="text/css" href="/css/styles.css">

</head>

<body>
<?php
	include 'header.php';
	include 'uitlezen.php';
	include 'footer.php';
?>
</body>

<script language="javascript" src='sorttable.js'></script>




