<?php 

	// Bewerk ondestaande parameters om wijzingen door te voeren
	$filename = 'wijzigingen_dezeweek.xml';  	// het pad naar de plek op de server waar de xml file kan worden gevonden
	$naamschool = 'Lyceum de Grundel';			// de weer te geven naam op de pagina
	$afkorting = 'twickel';						// afkorting gebruikt voor de opmaak van de pagina, kies alleen uit: twickel, grundel, avila of spindel (hoofdlettergevoelig!)

	

	// !! Laat onderstaande gegevens ongewijzigd !!

	// Local instellen
	setlocale(LC_TIME, 'nl_NL');

	// Tijdzone instellen
	date_default_timezone_set('Europe/Amsterdam');

	//Functies adresseren
	$wijzigingen = simplexml_load_file($filename);
	$datum = strftime("%A %e %B %Y");

	$startdatum = $wijzigingen->STARTDAG1;
	$startdatum = DateTime::createFromFormat(' Ymd ',$startdatum);
	$startdatum = $startdatum->format('m/d/Y');
	
	$einddatum = $wijzigingen->EINDDAG1;
	$einddatum = DateTime::createFromFormat(' Ymd ',$einddatum);
	$einddatum = $einddatum->format('d-m-Y');

?>