<?php	

	// Hier gebeurt het echte werk, het ophalen van de wijzingen uit de xml file

	// Eerst maken we een tabel aan	
	echo "<table class=sortable>
	<caption><div id='left'><h2>De volgende lessen wijzigen:</h2></div>
	<div id='right'><img src='/img/$afkorting/logo-tekst.png' id='logo'></div></caption>
	<tr><th>Datum</th><th>Klas</th><th>Uur</th><th>Vak</th><th>Docent</th><th>Lokaal</th><th>Wijzigingen</th></tr>";
		echo "<p>Voor het laatst bijgewerkt op: <b>" . date ("d-m-Y, H:i:s.", filemtime($filename));

	// vervolgens gaan we de xml uitlezen door telkens deze gegevens per item op te halen
	foreach ($wijzigingen->WIJZIGING as $info):
		$lesgroep = $info->SUBITEM->LESGROEP;
		$lesuur = $info->ORIGINEEL->LESUUR;
		$vak = $info->SUBITEM->VAK;
		$docent = $info->SUBITEM->DOCENT;
		$lokaal = $info->SUBITEM->LOKAAL;
		$soort = $info->WIJZIGINGSTYPE; 
		$dagnummer = $info->ORIGINEEL->DAGNUMMER -1;
		$dagnummer1 = $info->ORIGINEEL->DAGNUMMER;
		$dagnummer2 = $info->SUBITEM->DAGNUMMER;
		$verplaatsen = $info->ORIGINEEL->DAGNUMMER;
		$verplaatsen1 = $info->SUBITEM->DAGNUMMER;

		// Wijzigingscode herschrijven
		$soort = preg_replace("/1/", "Nieuwe les", $soort); 
		$soort = preg_replace("/2/", "Les vervalt", $soort);
		$soort = preg_replace("/3/", "Les verplaatst en/of vervangende docent", $soort);		

		// Dagnummer1 herschrijven
		$dagnummer1 = preg_replace("/1/", "ma", $dagnummer1); 
		$dagnummer1 = preg_replace("/2/", "di", $dagnummer1); 
		$dagnummer1 = preg_replace("/3/", "wo", $dagnummer1); 
		$dagnummer1 = preg_replace("/4/", "do", $dagnummer1); 
		$dagnummer1 = preg_replace("/5/", "vr", $dagnummer1); 	

		// Dagnummer2 herschrijven
		$dagnummer2 = preg_replace("/1/", "ma", $dagnummer2); 
		$dagnummer2 = preg_replace("/2/", "di", $dagnummer2); 
		$dagnummer2 = preg_replace("/3/", "wo", $dagnummer2); 
		$dagnummer2 = preg_replace("/4/", "do", $dagnummer2); 
		$dagnummer2 = preg_replace("/5/", "vr", $dagnummer2); 	

		// Hier bepalen we de orginele datum die hoort bij de mutatie. Dit doen we door de startdatum van de xml file te nemen en hier het 'dagnummer' uit de xml file bij op te tellen.
		$dag = date('d-m-Y',strtotime($startdatum . "+$dagnummer days"));

		// Als laatste moeten we er voor zorgen dat de wijzingen met de datum van vandaag een zwarte kleur krijgen, en de overigen een fel blauwe kleur.
		$kleur = "#000000";
	    if ($dag != date('d-m-Y'))
	        $kleur = "#0000ff";
	    else 
	        $kleur = "#000";

	    if ($verplaatsen != $verplaatsen1 && trim($verplaatsen1) != '' ) {
	    	$lesuur = "$dagnummer1 $lesuur >> $dagnummer2 $verplaatsen1";
	    	$dagnummer1 = "";
	    }

		// dit alles plaatsen we telkens in een tabelregel	
		echo "<tr style='color:$kleur'><td>",$dag,"</td><td>",$lesgroep,"</td><td>","$dagnummer1 $lesuur","</td><td>",$vak,"</td><td>",$docent,"</td><td>",$lokaal,"</td><td>",$soort,"</td></tr>";
	endforeach;

	// en dan sluiten we de tabel weer af
	echo "</table>";
	
	// Ophalen en weergeven wanneer het bestand voor het laatst op de server is bijgewerkt
	if (file_exists($filename)) {
		echo "</b></p>";
	}
?>